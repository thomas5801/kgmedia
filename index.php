<?php
include 'xyz/config.php';

if(!isset($_SESSION['user'])){
    require __DIR__ ."/ui/login.php";
    exit;   
}

$request = $_SERVER['REQUEST_URI'];

$request = explode("/",$request);

$params = null;
if(isset($request[2])){
	$params = $request[2];
}else{
	$params = $request[1];
}

$params = explode("?",$params);

if(isset($params[1])){
    $_gets = $params[1];
    $_gets = explode("&",$_gets);
    foreach($_gets as $vall ){
        $vall = explode("=",$vall);
        if(count($vall) == 2){
           $_get[$vall[0]] =  $vall[1];
        }
    }
}

$params = $params[0];
$_event_id =  $_SESSION['user']['Event'];



switch ($params) {
    case '' :
        header("Location: xyz/dashboard");
        break;
	case 'dashboard' :
        require __DIR__ . '/ui/dashboard.php';
        break;
    case 'transaction' :
        if(isset($request[3]) AND $request[3] != ""){            
            require __DIR__ . '/ui/invoice.php';
            break;
        }
        require __DIR__ . '/ui/transaction.php';
        break;
	case 'participants' :
        require __DIR__ . '/ui/participants.php';
        break;
	case 'coupons' :
        require __DIR__ . '/ui/coupons.php';
        break;
	case 'ticket' :
        require __DIR__ . '/ui/ticket.php';
        break;
    case 'order' :
        require __DIR__ . '/ui/order.php';
        break;
    case 'proses' :
        require __DIR__ . '/ui/proses.php';
        break;
    case 'dikirim' :
        require __DIR__ . '/ui/dikirim.php';
        break;
    case 'tiba' :
        require __DIR__ . '/ui/tiba.php';
        break;
	case 'produk' :
        require __DIR__ . '/ui/produk.php';
        break;
    case 'timeline' :
        require __DIR__ . '/ui/timeline.php';
        break;
	case 'resend' :
        require __DIR__ . '/xyz/resend.php';
        break;
    case 'aksi_order' :
        require __DIR__ . '/xyz/aksi_order.php';
        break;
    case 'aksi_proses' :
        require __DIR__ . '/xyz/aksi_proses.php';
        break;
    case 'aksi_tiba' :
        require __DIR__ . '/xyz/aksi_tiba.php';
        break;
    case 'event' :
        if(isset($request[3])) change_event($request[3]);
        require __DIR__ . '/xyz/event.php';
        break;
    default:
        break;
}


function change_event($event) {
    if((int)$event <> 0 ) {
        foreach($_SESSION['user']['Events'] as $vall){
            if($vall->evnhId == $event){
                
                
                $_SESSION['user']['Event']      = $vall->evnhId;
                $_SESSION['user']['EventName']  = $vall->evnhName;
            }
        }
    }
}

?>